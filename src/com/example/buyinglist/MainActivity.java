package com.example.buyinglist;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		BuyingItemDB.Initalize(getApplicationContext());
		List<BuyItem> items = BuyingItemDB.getAllBuyingItem();
		ShoppingList.Init(items);
		
		text = (EditText)findViewById(R.id.editText1);
		Button button = (Button)findViewById(R.id.bAddItem);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast toast = Toast.makeText(getApplicationContext(), "Added" , 2);
				toast.show();
				BuyItem item = ShoppingList.addEntry(text.getText().toString());
				BuyingItemDB.insertBuyingItem(item);
			}
		});
		
		Button showListButton = (Button)findViewById(R.id.bShowList);
		showListButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent listIntent = new Intent(getApplicationContext(), ItemListActivity.class);
				startActivity(listIntent);
			}
		});
		
		TextView view = (TextView)findViewById(R.id.textView1);
		GetTimeTask task = new GetTimeTask();
		task.execute(view);
	}

	    	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/
	
	
	class GetTimeTask extends AsyncTask<TextView, Integer, String>{
		private TextView timeView;
		
		@Override
		protected String doInBackground(TextView... params) {
			timeView = params[0];
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet get = new HttpGet("http://www.earthtools.org/timezone-1.1/60.795430/10.691630");
			HttpResponse response;
			try {
				response = httpClient.execute(get);
				String result = EntityUtils.toString(response.getEntity());
				
				 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			     DocumentBuilder builder = factory.newDocumentBuilder();
			     InputSource is = new InputSource(new StringReader(result));
			     Document doc =  builder.parse(is);
			     return doc.getElementsByTagName("localtime").item(0).getTextContent();
			} catch (Exception e) {
				Log.e("HTTP",e.toString());
			}

			return "Can't connect";
		}
		
		@Override
		 protected void onPostExecute(String address) {
			timeView.setText(address);
		 }
	}
	
	
}
