package com.example.buyinglist;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BuyingItemDB extends SQLiteOpenHelper{
	private static BuyingItemDB instance;
	private static final String DATABASE_NAME = "ShoppingListStorage";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CREATE =
                "CREATE TABLE BuyingItems (ID INTEGER, Content TEXT)";
    
    public BuyingItemDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void Initalize(Context context){
    	instance = new BuyingItemDB(context);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
    	db.execSQL(TABLE_CREATE);
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE BuyingItems");	
	
		db.execSQL(TABLE_CREATE);
	}
	
	
	public static List<BuyItem> getAllBuyingItem(){
		SQLiteDatabase Db = instance.getReadableDatabase();
		List<BuyItem> buyingItems = new ArrayList<BuyItem>();
		
		Cursor curs = Db.query("BuyingItems", null, null, null, null, null, null);
		
		while (curs.moveToNext()){
			BuyItem buyingItem = new BuyItem(curs.getInt(0), curs.getString(1));
			buyingItems.add(buyingItem);
		}
		
		Db.close();
		return buyingItems;
	}
	
	public static void insertBuyingItem(BuyItem item){
		SQLiteDatabase Db = instance.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("ID", item.id);
		values.put("Content", item.content);
		Db.insert("BuyingItems",  null, values);
		Db.close();
	}
	
	public static void deleteBuyingItem(BuyItem item){
		SQLiteDatabase Db = instance.getWritableDatabase();
		Db.delete("BuyingItems", "ID = " + item.id, null);
		Db.close();
	}
}
