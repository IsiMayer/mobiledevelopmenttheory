package com.example.buyinglist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ItemDetailFragment extends Fragment {
	public static final String ARG_ITEM_ID = "item_id";

	private BuyItem mItem;

	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = ShoppingList.List.get(getArguments().getInt(
					ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);


		if (mItem != null) {
			((TextView) rootView.findViewById(R.id.item_detail))
					.setText(mItem.content);
		}

		Button button = (Button)rootView.findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				ShoppingList.List.remove(mItem.id);
				BuyingItemDB.deleteBuyingItem(mItem);
				Intent listIntent = new Intent(getActivity().getApplicationContext(), ItemListActivity.class);
				startActivity(listIntent);
			}
		});
		return rootView;
	}
}
