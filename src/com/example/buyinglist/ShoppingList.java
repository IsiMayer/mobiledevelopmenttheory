package com.example.buyinglist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingList {
	private static int nextID = 0;
	
	public static Map<Integer, BuyItem> List = new HashMap<Integer, BuyItem>();

	/*static {
		// Add 3 sample items. 
		addItem(new BuyItem(nextID++, "Kuchen"));
		addItem(new BuyItem(nextID++, "Milch"));
		addItem(new BuyItem(nextID++, "Zucker"));
	}*/

	public static BuyItem addEntry(String content){
		BuyItem item = new BuyItem(nextID++, content);
		addItem(item);
		return item;
	}
	
	private static void addItem(BuyItem item) {
		List.put(Integer.valueOf(item.id), item);
	}

	public static void Init(List<BuyItem> items) {
		for(BuyItem item : items){
			if (item.id >= nextID){
				nextID = item.id + 1;
			}
			addItem(item);
		}
	}
}
