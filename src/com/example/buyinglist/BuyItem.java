package com.example.buyinglist;

public class BuyItem {
		public int id;
		public String content;

		public BuyItem(int id, String content) {
			this.id = id;
			this.content = content;
		}

		@Override
		public String toString() {
			return content;
		}
	}